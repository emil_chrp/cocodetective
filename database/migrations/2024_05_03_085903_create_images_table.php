<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * - ETML
 * - Author : Emilien Charpié
 * - Created at : 02.05.2024
 * - Updated at : 06.05.2024
 *
 * - Description : This file is the Laravel migration file. It will create a table in the databas
 * for store the images that have been analysed by AI.
*/


return new class extends Migration
{
    /**
     * Create the "images" table
     */
    public function up(): void
    {
        /**
         *Create the table with the id, the path of the image, the classes of the image, the id of the user that upload the image and the timestamps
         */
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->string('image_path');
            $table->string('image_classes');
            $table->foreignId('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('images');
    }
};
