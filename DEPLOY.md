# Manuel de déploiement

Comment mettre l'application en production

## Manuel de déploiement
Ce manuel est le manuel de déploiement. Pour modifier l'application, il faut suivre le [manuel de développement](README.md).

Avant de commencer, il vous faudra une connexion en ssh avec un droit d'écriture à votre serveur de production.

Ensuite, assurez-vous d'avoir [PHP](https://www.php.net/downloads.php), [Composer](https://getcomposer.org/) et [Node.js](https://nodejs.org/en/download) installé sur votre serveur.

### Télécharger l'application

Si tout cela est installé, vous pouvez cloner ce repository sur votre serveur à l'endroit où les utilisateur seront redirigés lorsqu'il visite le site :
```
git clone https://gitlab.com/emil_chrp/cocodetective
```

>Sous Linux, la plupart du temps, le dossier dont l'utilisateur aura accès depuis un navigateur est le dossier /var/www/. En revanche, cela peut changer selon les OS ou les serveurs Web. Il est recommandé de vérifier la documentation de votre serveur ou de votre OS pour savoir où installer l'application.

### Configurer PHP correctement

Cette application utilise beaucoup de mémoire et nécessite des modules php pour fonctionner correctement. Tout d'abord, vous devez localiser le fichier « php.ini ». Vous pouvez localiser ce fichier avec la commande suivante :
```
php --ini
```

Ensuite, ouvrez le fichier et recherchez la ligne `memory_limit`. Modifiez ensuite la valeur par 1024 :
```
memory_limit=1024M
```

Maintenant, il faut changer la valeur du temps d'exécution maximum, car le modèle prend du temps a exécuter le modèle. Pour se faire, il faut changer la valeur de `max_execution_time` :
```
max_execution_time=180
```

Ensuite, vous devez activer ffi. Pour ce faire, vous pouvez simplement ajouter une ligne dans le fichier `php.ini` :
```
ffi.enable=true
```

Et puis, assurez-vous que gd est installé en php. Si vous n'avez pas php gd, vous pouvez l'installer.

Sous Linux :
```
sudo apt-get install php-gd
```

>Veuillez noter que pour Linux, cela peut ne pas fonctionner en exécutant uniquement la ligne de commande. Si cela ne fonctionne pas, suivez simplement la même étape qu'un utilisateur Windows

Sous Windows :
Sous Windows, vous devez localiser sur le fichier `php.ini` la ligne `;extension=gd` et supprimer le `;`. Si vous n'avez pas cette ligne, vous pouvez simplement ajouter cette ligne dans le fichier :
```
extension=gd
```

>Veuillez noter que GD peut déjà être installé sur la version par défaut de php, je recommande donc d'exécuter l'application en premier, et s'il y a une erreur, essayez d'installer GD

### Configurer Laravel

Pour cela, vous devez être dans le dossier `Coco_Detective`. Vous pouvez y naviguer avec la commande suivante :
```
cd cocodetective
```

Installer composer via un terminal de commande sous Linux :
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --filename=composer
php -r "unlink('composer-setup.php');"
```

Mettre à jour les dépendance de composer :
```
composer update
```

Installez les dépendances PHP via Composer :
```
composer install --no-dev --optimize-autoloader
```

Ensuite, il faut installer OnnxRuntime pour PHP avec les commandes suivantes :
```
composer require ankane/onnxruntime
composer exec -- php -r "require 'vendor/autoload.php'; OnnxRuntime\Vendor::check();"
```

Installez npm :
```
sudo apt-get install npm
```

Installez les dépendances JavaScript via npm :
```
npm install
```

Puis, créez compilez le style :
```
npm run build
```

Créez un fichier `.env` en copiant le fichier `.env.example`. Vous pouvez le faire avec la commande suivante :
```
cp .env.example .env
```

Une fois fait, il faut modifier plusieurs valeurs dans ce nouveau fichier `.env`. Les valeurs à modifier sont les suivantes :

- `APP_ENV` doit être définie à `production`, car elle va permettre à Composer de savoir quelles dépendances installer
- `APP_DEBUG` doit être définie à `false`, car nous ne voulons pas les erreurs Laravel en production
- `APP_URL` doit contenir l'url de l'application Laravel

Ensuite, il faut modifier les informations relatives à la base de donnée :

- `DB_CONNECTION` doit être définie avec le type de base de donnée que l'application utiliser. Par exemple `mysql`
- `DB_HOST` sert à définir l'adresse du serveur où se trouve la base de données
- `DB_PORT` sert à définir le port sur lequel la connection devra être établie
- `DB_DATABASE` sert à définir le nom de la base de données 
- `DB_USERNAME` sert à définir le nom d'utilisateur pour pouvoir se connecter à la base de données
- `DB_PASSWORD` sert à déinir le mot de passe de l'utilisateur de la base de données

Ensuite, il faut laisser la valeur de `APP_KEY` vide. Il faut générer la valeur avec la commande suivante :
```
php artisan key:generate
```

Une fois que le fichier `.env` a été modifié avec les bonnes valeurs, il faut exécuter les migrations de bases de données :
```
php artisan migrate
````

Ensuite, liez le stockage, vous aurez ainsi les ressources nécessaires pour exécuter l'application :
```
php artisan storage:link
```

Puis, il faut attribuer à l'utilisateur Web les droit en écriture pour le dossier `storage` avec la commande suivante :
```
sudo chown -R username:username /var/www/cocodetective/storage
```

### Configuration du serveur

Pour que l'application fonctionne correctement, il faut configurer votre serveur web pour qu'il redirige les utilisateurs Web dans le dossier `public` de l'application. Ce dossier se trouve au chemin `Coco_Detective/public`. 

>Veuillez notez que la procédure est peut être la même pour d'autres serveurs Web, toutefois, elle peut différer selon chaques serveurs et par conséquent, il est impossible de faire une liste exhaustive. C'est pourquoi nous n'allons parler uniquement de la configuration d'un serveur web Nginx.

Dans un serveur Nginx, pour pouvoir faire ensorte de rediriger l'application au bon endroit sur le serveur, il faut créer un fichier de configuration dans le dossier `site-available` pour le faire, il faut se rendre dans le dossier d'Nginx. Sous linux la commande suivante le permet :
```
cd /etc/nginx
```

Ensuite, nous pouvons créer et éditer un fichier nommé `coco.conf` dans le dossier `site-available` qui servira de confguration pour notre application :
```
touch site-available/coco.conf
nano site-available/coco.conf
```

Une fois fait, il faut copier le code suivant dans le fichier de configuration créer :
```
server {
    listen 80 default_server;
    server_name www.example.com;
    root /var/www/example-website;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";

    index index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.2-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
```

Ce code permer de définir le port `80` comme port d'écoute pour le serveur possédant le nom `www.example.com`. Et le dossier root est le dossier où l'utilisateur va être redirigé lorsqu'il visitera le serveur `www.example.com`, à savoir `/var/www/example-website`.

>Sous Linux, le dossier qui est autorisé à être vu depuis un navigateur Web est la plupart du temps /var/www/, c'est pourquoi le site a besoin d'être héberger à cet endroit. En revanche, ce dossier peut changer en fonction des serveurs ou des OS, alors il faut vous renseigner sur la documentation de celui que vous utilisez.

Ensuite, il faut activer la configuration que l'on vient de faire sur le serveur avec la commande : 
```
sudo ln -s /etc/nginx/sites-available/coco.conf /etc/nginx/sites-enabled/
```

### Optimisation de l'application

Une fois que l'application a été installée et configurée, il faut optimiser tous les fichiers pour qu'elle fonctionne le mieux possible.

Pour commencer, il faut exécuter la commande suivante : 
```
php artisan optimize
```

Ensuite, il faut mettre en cache toutes les informations de configuration :
```
php artisan config:cache
```

Puis, les evenements :
```
php artisan event:cache
```

Ensuite, il faut mettre en cache les routes :
```
php artisan route:cache
```

Puis, il faut également cacher les vues : 
```
php artisan view:cache
```

Une fois que toutes ces informations ont été mises en cache, l'application est optimisée.

## Dépendances

- [PHP](https://www.php.net/downloads.php)
- [Laravel](https://laravel.com/)
- [ONNXRuntime pour PHP](https://github.com/ankane/onnxruntime-php)
- [Compositeur](https://getcomposer.org/)
- [Node.js](https://nodejs.org/en/download)
