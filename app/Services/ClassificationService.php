<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

/**
 * - ETML
 * - Author : Emilien Charpié
 * - Created at : 02.05.2024
 * - Updated at : 06.05.2024
 *
 * - Description : This file is the service of the analysation with AI models.
 * It contain all the metods to analyse an image or a zip file.
 */

class ClassificationService
{
    /**
     * Detect if the file is an image or a zip and then call method to unzip or classify
     * Params :
     * - $filepath : The path of the uploaded file
     * - $file_extension : The extension of the uploaded file
     * Return :
     * - An array that contain all the informations about all the images analysed
     */
    public function classify($filePath, $fileExtension)
    {
        /**
         * This array is the same that in the Controller. It will contain all the informations of the results for each images
         * There is a key for each image -> $responses[1] & $responses[2]
         * For each images there is the number of there classes, the box where the class has been found,
         *     and an array that cointain the classes as text, and some text
         *     -> $responses[1]['classNames']['class'] & $responses[1]['classNames]['text']
         * The ['classNames']['class'] is an array of all the classes that the onnx model have found
         * The ['classNames']['text'] is all the informations that can be usefull for each images (example : "This image cannot be analysed")
         */
        $responses = [];

        // Check if the file uploaded is a jpeg, jpg or zip
        switch($fileExtension){
            case 'jpg':
            case 'jpeg':
                // Classify the image
                $responses = $this->classifyImage($filePath);

                return $responses;

            case 'zip':
                // Create a zip temp folder if it has been deleted
                if(!file_exists(public_path('storage/uploaded_images/zip'))){
                    mkdir(public_path('storage/uploaded_images/zip'));
                }
                // Classify all the images of a zip folder
                $responses = $this->classifyZip($filePath);

                // Remove the zip temp folder
                rmdir(public_path('storage/uploaded_images/zip'));

                return $responses;

            default:
                // If the user has not uploaded a jpg, jpeg or zip file
                $responses[0] = 'error';
                $responses[1] = "Vous avez téléverser un fichier ." . $fileExtension . ". Seuls les fichiers .jpg, .jpeg ou .zip sont pris en charge";

                return $responses;
        }
    }

    /**
     * Classify a jpg or jpeg image
     * Params :
     * - $img_path : The path of the image
     * Return :
     * - The classes and some userfull informations for the analysed image
     */
    private function classifyImage($imgPath)
    {
        /** This array will contain all the informations of the image.
         * The classes, the classes numbers, the number of elements founds and the boxes
         */
        $responses = [];

        $img = imagecreatefromjpeg($imgPath);

        $pixels = $this->getPixelArray($img);

        $responses[0] = $this->runOnnx($pixels);

        // For each classes that have been found, draw a square where the elements has been detected on the image
        foreach ($responses[0]['classNames']['class'] as $key => $class) {
            $this->drawSquare($img, $responses[0]['classNames']['class'][$key]['name'], $responses[0]['boxResults'][$key]);
        }

        $responses[0]['imagePath'] = $this->storeImage($img);

        return $responses;
    }

    /**
     * Classify all pictures in a zip folder
     * Params :
     * - $zip_path : The path of the zip folder
     * Return :
     * - An array with the classes and some userfull informations for each images
     */
    private function classifyZip($zipPath)
    {
        /** This array will contain all the responses for each Images
         * For example, the first image will have it's informations in $responses[0][...] and the second will have its informations in $responses[1][...]
         *
         * For each images there is the classes, the classes numbers, the number of elements founds and the boxes as informations
         */
        $responses = [];

        // Unzip the zip file as an array of files
        $files = $this->unzip($zipPath);

        $folder = array_shift($files);

        // For each file in the zip folder, get the pixel array, run the onnx model and store it on the server
        for ($i = 0; $i < count($files); $i++) {
            // Get the extention of the file to find if there is a jpg or a jpeg
            $extention = pathinfo($files[$i]['path'], PATHINFO_EXTENSION);

            // Check if the file is a jpg or a jpeg and then get the pixel array, run the onnx model and stor the image to the server
            if($extention == "jpg" || $extention == "jpeg"){
                $img = imagecreatefromjpeg($files[$i]['path']);

                $pixels = $this->getPixelArray($img);

                $responses[$i] = $this->runOnnx($pixels);

                // For each object detected, draw a square where the elements has been detected on the image
                foreach ($responses[$i]['classNames']['class'] as $key => $class) {
                    $this->drawSquare($img, $responses[$i]['classNames']['class'][$key]['name'], $responses[$i]['boxResults'][$key]);
                }

                $responses[$i]['imagePath'] = $this->storeImage($img);
            } else {
                // Return an error
                $responses = [];
                $responses[0] = 'error';
                $responses[1] = "Le fichier zip contient des images non suportées par le modèle ONNX. Il faut que toutes les images soient des .jpg ou des .jpeg.";

                // Remove the temporary files that the zip have created
                $this->removeTempFiles($files, $folder);

                return $responses;
            }
        }

        // Remove the temporary files that the zip have created
        $this->removeTempFiles($files, $folder);

        return $responses;
    }

    /**
     * Make a rgb array of pixels from an image
     * Params :
     * - $img : the image in gd format that will be separated in pixel array
     * Return :
     * - An array of pixel
     */
    private function getPixelArray($image){
        // The array that will contain all the pixels
        $pixels = [];
        $width = imagesx($image);
        $height = imagesy($image);

        // For each columns, get the rows, and then for each pixels get the rgb and store it to an array
        for ($y = 0; $y < $height; $y++) {
            $row = [];
            for ($x = 0; $x < $width; $x++) {
                $rgb = imagecolorat($image, $x, $y);
                $color = imagecolorsforindex($image, $rgb);
                $row[] = [$color['red'], $color['green'], $color['blue']];
            }
            $pixels[] = $row;
        }

        return $pixels;
    }

    /**
     * Run the onnx model "mobilenet" to classify a pixel array
     * Params :
     * - $pixel_array : A rgb pixel array
     * Return :
     * - An array with the classes and the userfull informations of an image that have been classed
     */
    private function runOnnx($pixels)
    {
        $results = [
            'classNumbers' => [],
            'classNames' => [],
            'boxResults' => []
        ];

        // Get the onnx model
        $model = new \OnnxRuntime\Model(Storage::path('/public/onnx-models/model.onnx'));

        $modelResponses = $model->predict(['inputs' => [$pixels]]);
        $allClasses = $modelResponses["detection_classes"];
        $numberOfResults = $modelResponses["num_detections"][0];
        $allBoxes = $modelResponses['detection_boxes'];

        // Get the index and the classes that the model found
        $results['classNumbers'] = array_slice($allClasses[0], 0, $numberOfResults);
        $results['classNames'] = $this->getClasses($results['classNumbers']);


        // Get the boxes that the model founds
        $results['boxResults'] = array_slice($allBoxes[0], 0 ,$numberOfResults);

        return $results;
    }

    /**
     * Draw a square on the image where the model has detected some elements
     * Params :
     * - $img : this image that the model have to analyse
     * - $label : the label of the image
     * - $box : the location of the box found by the model
     */
    private function drawSquare(&$img, $label, $box){
        // Get the box locations
        $width = imagesx($img);
        $height = imagesy($img);

        $top = round($box[0] * $height);
        $left = round($box[1] * $width);
        $bottom = round($box[2] * $height);
        $right = round($box[3] * $width);

        // Draw box
        $red = imagecolorallocate($img, 255, 0, 0);
        imagerectangle($img, $left, $top, $right, $bottom, $red);

        // Draw text
        $font = public_path('storage/fonts/Roboto-Black.ttf');
        imagettftext($img, 16, 0, $left, $top - 5, $red, $font, $label);
    }


    /**
     * Get the class names from the coco dataset of an image
     * Params :
     * - $classNumbers : the numbers of the classes that the model founds
     * Return :
     * - An array with the names of each class that the model founds
     */
    private function getClasses($classNumbers){
        // The array that will contain the class and the text for each image
        $responses = [
            'text' => "",
            'class' => []
        ];

        // Load the json file with the responses as a php array
        $cocoData = file_get_contents(Storage::path('/public/onnx-models/coco-label-paper-all.json'));
        $cocoLabels = json_decode($cocoData, true);


        if ($cocoLabels === null) {
            $responses['text'] = "An error occured when we try to decode the answers</br>";
            exit(1);
        }

        $class = [];

        // Get trough the classes that the model have found
        for ($x = 0; $x < count($classNumbers); $x++) {
            // Get trough the coco classes and see which one is corresponding
            for ($y = 0; $y < count($cocoLabels); $y++) {
                if($cocoLabels[$y]['id'] == $classNumbers[$x]){
                    $class[$x]['name'] = $cocoLabels[$y]['name'];
                    $class[$x]['category'] = $cocoLabels[$y]['category'];
                }
            }
        }

        // Set the answer array
        $responses['class'] = $class;

        return $responses;
    }

    /**
     * Store an image to the server and then return the path
     * Params :
     * - $img : the image
     * Return :
     * - The path of the image stored
     */
    private function storeImage($img){
        // Set all the informations of the image path
        $fileName = time().'.jpg';
        $pathToStore = public_path('storage/uploaded_images/');
        $returnedPath = 'storage/uploaded_images/';

        // Create the image and destroy the object created before
        imagejpeg($img, $pathToStore . $fileName);
        $this->destroy($img);

        return $returnedPath . $fileName;
    }

    /**
     * Unzip a folder
     * Params :
     * - $path : The path of the zip folder
     * Return :
     * - An array with the path of the unzipped files
     */
    private function unzip($path)
    {
        // Set an empty array that will contain all the informations for the unzipped files
        $files = [];

        // Unzip the folder and extract the files to a specific location
        $zip = new \ZipArchive;
        if ($zip->open($path) === true) {
            for($i = 0; $i < $zip->numFiles; $i++) {
                $files[$i]['name'] = $zip->getNameIndex($i);
                $files[$i]['info'] = pathinfo($files[$i]['name']);
            }
            $zip->extractTo('../public/storage/uploaded_images/zip');
            $zip->close();
        }

        // Set an array of files, with all the userfull informations
        for ($i=0; $i < count($files); $i++) {
            if($i == 0){
                $files[$i]['path'] = "../public/storage/uploaded_images/zip/" . $files[$i]['info']['basename'];
            } else {
                $files[$i]['path'] = "../public/storage/uploaded_images/zip/" . $files[$i]['name'];
                switch($files[$i]['info']['extension']){
                    case 'jpeg':
                    case 'jpg':
                        $files[$i]['type'] = "jpg";
                        break;
                    default:
                        $files[$i]['type'] = $files[$i]['info']['extension'];
                        break;
                }
            }
        }

        // Set an array that will contain the files usable
        $usefullFiles = [];

        // Set an array with the files that needs to be deleted
        $uselessFiles = [];

        // Set the index for the usefull_files array
        $indexUsefullFiles = 0;

        // Set the index for the useless_files array
        $indexUselessFiles = 0;

        // Remove all the useless files that are created when the folder has been unzipped
        for ($i=0; $i < count($files); $i++) {
            // If this is the parent folder, copy it
            if($i==0){
                $usefullFiles[$indexUsefullFiles] = $files[$i];
                $indexUsefullFiles++;
            } else {
                // Set the string for the dir to delete
                $excludeDir = "__MACOSX/".$files[0]['info']['basename'];

                // Check if the file is not from the __MACOSX folder
                if($files[$i]['info']['dirname'] != $excludeDir){
                    $usefullFiles[$indexUsefullFiles] = $files[$i];
                    $indexUsefullFiles++;
                } else {
                    $uselessFiles[$indexUselessFiles] = $files[$i];
                    $indexUselessFiles++;
                }
            }
        }

        // Remove all the useless files
        foreach($uselessFiles as $file){
            unlink($file['path']);
        }

        // Remove the two useless directories of the mac zipped folder if they exists
        if(is_dir('../public/storage/uploaded_images/zip/__MACOSX/'.$usefullFiles[0]['info']['basename'])){
            rmdir('../public/storage/uploaded_images/zip/__MACOSX/'.$usefullFiles[0]['info']['basename']);
        }
        if(is_dir('../public/storage/uploaded_images/zip/__MACOSX')){
            rmdir('../public/storage/uploaded_images/zip/__MACOSX');
        }

        return $usefullFiles;
    }

    /**
     * Remove unzipped files
     * Params :
     * - $files : an array of all the files that needs to be removed
     */
    public function removeTempFiles(array $files, array $folder){
        // Remove the files & the directory created when we unzipped the folder
        for ($i=0; $i < count($files); $i++) {
            unlink($files[$i]['path']);
        }
        rmdir($folder['path']);
    }

    /**
     * Remove the specified resource from storage.
     * Params :
     * - $file : the file to destroy
     */
    private function destroy($file)
    {
        imagedestroy($file);
    }

}
