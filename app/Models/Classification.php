<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * - ETML
 * - Author : Emilien Charpié
 * - Created at : 02.05.2024
 * - Updated at : 06.05.2024
 *
 * - Description : This file is the Model of the classification service.
 * It store the analysed images on the database
 */


class Classification extends Model
{
    use HasFactory;

    protected $table = "images";

    // Set the fillable informations for the insertions in database
    protected $fillable = [
        "image_path",
        "image_classes",
        "user_id"
    ];
}
