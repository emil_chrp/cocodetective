<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Client\Response;
use ZipArchive;

class DownloadController extends Controller
{
    public function download(){
        // Set all the variables for the path and the file names
        $imagesInformations = json_decode(request('imagesInformations'), true);
        $newPath = public_path('storage/downloaded_images/');
        $folderName = time();
        $folderPath = $newPath.$folderName;
        $zipFolderName = $folderName.'.zip';
        $zipFolderPath = $newPath.$zipFolderName;

        // Create a new directory for copy the images in it
        mkdir($folderPath);

        foreach ($imagesInformations as $key => $information) {
            $imageName = 'download_'.$key.'.jpg';
            copy($information['imagePath'], $folderPath.'/'.$imageName);
        }

        // Create a zip file and add all the file in it
        fopen($zipFolderPath, "w");

        $zip = new ZipArchive;
        $zip->open($zipFolderPath, ZipArchive::CREATE);
        foreach (glob($folderPath .'/*') as $file) {
            $zip->addFile($file, basename($file));
        }
        $zip->close();

        // Delete the folder where was coping the files and only keep the zip file
        foreach ($imagesInformations as $key => $information) {
            $imageName = 'download_'.$key.'.jpg';
            unlink($folderPath.'/'.$imageName);
        }
        rmdir($folderPath);

        // Download the zip for the user
        return response()->download($zipFolderPath);
    }

    public function success(){
        return view('success-download');
    }
}
