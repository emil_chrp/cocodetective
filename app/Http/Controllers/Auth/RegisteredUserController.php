<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:'.User::class, 'regex:/^[a-zA-Z0-9._%+-]+@eduvaud\.ch|oberson\.bernard@gmail\.com|michael\.wyssa@eduvaud\.ch$/'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ],
        [
            'email.regex' => "L'email doit être du format xxx@eduvaud.ch",
            'email.lowercase' => "L'email doit être ecris en minuscule",
            'password.min' => "Le mot de passe doit faire au minimum 8 caractères",
            'password.confirmed' => "Les mots de passes ne correspondent pas",
            'email.unique' => "Cet email est déjà utilisé. Essayez de vous connecter à la place",
            'name.max' => 'le nom ne peut pas dépasser 255 caractères',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
