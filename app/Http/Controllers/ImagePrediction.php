<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Services\ClassificationService;
use App\Models\Classification;
use \Exception;

/*
 * - ETML
 * - Author : Emilien Charpié
 * - Created at : 02.05.2024
 * - Updated at : 06.05.2024
 *
 * - Description : This file is the Controller of the prediction service.
 * It link the view, that display forms, the model, that store images classed on db and the service, that
 * analyse images with AI
 */

class ImagePrediction extends Controller
{
    // The classification object from the ClassificationService class
    public $classification;

    /**
     * This is the constructor of the Controller
     * It initialize the ClassificationService Class
     * Params :
     * - $ClassificationService : This is the class of the ClassificationService
     */
    public function __construct(ClassificationService $ClassificationService)
    {
        $this->classification = $ClassificationService;
    }

    /**
     * Display the page to upoad an image
     */
    public function uploadForm()
    {
        return view('classify');
    }

    /**
     * Classify the image with the onnx model when a user have submited the form
     * Params :
     * - $request : The request when the form has been submitted
     * Return :
     * - The view "image-classed"
     */
    public function classify(Request $request)
    {
        //Check if the user is connected or if he has not uploaded an image since the last 60 seconds
        if(Auth::check() || !Auth::check() && $this->isAllowed()){

            /**
             * This array will contain all the informations about the images analysed.
             * There is a key for each image -> $response[1] & $response[2]
             * For each images there is some classes and some text -> $response[1]['class'] & $response[1]['text]
             * The class is an array of all the classes that the onnx model have found
             * The text is all the informations that can be usefull for each images (example : "This image cannot be analysed")
             */
            $responses = [];

            // Check if the user have uplaod a file and return an error that will be displayed on the view if not
            if(empty(request('file'))){
                return view('classify', [
                    "error" => "Vous devez téléverser un fichier"
                ]);
            }

            // Get the informations of the uploaded file
            $fileExtension = request('file')->getClientOriginalExtension();
            $filePath = request('file');

            if($fileExtension == "zip" && !Auth::check()){
                return view('classify', [
                    "error" => "Vous devez être connecté pour téléverser un fichier zip"
                ]);
            }

            // Analyse the file with the ClassificationService
            $responses = $this->classification->classify($filePath, $fileExtension);

            if($responses[0] == 'error'){
                return view('classify', [
                    "error" => $responses[1]
                ]);
            }

            // Try to store the image on the db to see if the request is too large
            try {
                // Store the path of the image, the classes that have been found and the id on the database
                $this->store($request, $responses);
              }

            catch(Exception $e) {
                return view('classify', [
                    "error" => "L'image que vous souhaitez analyser contient trop d'informations"
                ]);
            }

            // Update the time when the user upload an images
            if(!Auth::check()){
                $this->updateAllowed();
            }

            // Return the view with all the informations in the $response array
            return view('image-classed', [
                "responses" => $responses
            ]);

        // If the user is not logged and he have already upload a file in the last 60 seconds
        } elseif (!Auth::check() && !$this->isAllowed()) {
            return view('classify', [
                "error" => "Vous n'êtes pas connectés et devez attendre 1 minute avant de refaire analyser un fichier"
            ]);
        }
    }

    /**
     * Store the image in the batabase with differrents attributes
     * Images path on the server
     * Images classes
     * Id of the user who uploaded the image -> (null if the user was not logged)
     * Params
     * - $request : The request of the submitted form
     * - $response : The response of the AI classification for an image
     */
    public function store(Request $request, $responses)
    {
        $file = $request->file('file');

        //Validate the request, because the code handle only jpeg, jpg and zip
        $request->validate([
            'file' => 'mimes:jpg,jpeg,zip'
        ]);

        // Check if the user that upload the file was logged, if he was not, set the id to "null"
        if(Auth::check()){
            $userId = auth()->user()->id;
        }else{
            $userId = null;
        }

        // If the uploaded file is a .zip file
        if($file->getClientOriginalExtension() == "zip"){

            foreach ($responses as $key => $value) {
                if($key > 0){
                    Classification::create([
                        'image_path' => $responses[$key]['imagePath'],
                        'image_classes' => json_encode($responses[$key]['classNames']),
                        'user_id' => $userId
                    ]);
                }
            }

        // If the user have uploaded a file, store it in the database
        } else {
            Classification::create([
                'image_path' => $responses[0]['imagePath'],
                'image_classes' => json_encode($responses[0]['classNames']),
                'user_id' => $userId
            ]);
        }
    }

    /**
     * Check if a user can upload a file
     * Return :
     * - True if the user can upload a file, false if the user cannot
     */
    protected function isAllowed() {
        $lastSubmit = request()->session()->get('lastSubmit');
        return ($lastSubmit < (time() - 60));
     }

    /**
     * Update when was the last upload of a user
     */
     protected function updateAllowed() {
        request()->session()->put('lastSubmit', time());
     }
}
