# Coco Detective

Faites analyser vos images avec l'intélligence artifielle

## Qu'est-ce que Coco Detective ?
Coco Detective est une application Web Laravel qui utilise un modèle ONNX pour classer des images. Cette application vous permet de télécharger une image, puis utilise un modèle d'apprentissage pré-entraîné pour identifier et classer les objets présents dans cette image. Les images sont ensuite formattées avec un carré rouge pour voir où le modèle a identifier les informations.

## Manuel de développement
Ce manuel est le manuel de développement. Pour mettre en production l'application, il faut suivre le [manuel de déploiement](DEPLOY.md).

Avant de commencer, assurez-vous d'avoir [PHP](https://www.php.net/downloads.php), [Composer](https://getcomposer.org/) et [Node.js](https://nodejs.org/en/download) installé sur votre machine.

### Télécharger l'application

Si tout cela est installé, vous pouvez cloner ce répository sur votre machine locale :
```
git clone https://gitlab.com/emil_chrp/cocodetective
```

### Configurer PHP correctement

Cette application utilise beaucoup de mémoire et nécessite des modules php pour fonctionner correctement. Tout d'abord, vous devez localiser le fichier « php.ini ». Vous pouvez localiser ce fichier avec la commande suivante :
```
php --ini
```

Ensuite, ouvrez le fichier et recherchez la ligne `memory_limit`. Modifiez ensuite la valeur par 1024 :
```
memory_limit=1024M
```

Maintenant, il faut changer la valeur du temps d'exécution maximum, car le modèle prend du temps a exécuter le modèle. Pour se faire, il faut changer la valeur de `max_execution_time` :
```
max_execution_time=180
```

Ensuite, vous devez activer ffi. Pour ce faire, vous pouvez simplement ajouter une ligne dans le fichier `php.ini` :
```
ffi.enable=true
```

Et puis, assurez-vous que gd est installé en php. Si vous n'avez pas php gd, vous pouvez l'installer.

Sous Linux :
```
sudo apt-get install php-gd
```

Sur Mac avec Brew :
```
brew install php-gd
```

>Veuillez noter que pour Mac et Linux, cela peut ne pas fonctionner en exécutant uniquement la ligne de commande. Si cela ne fonctionne pas, suivez simplement la même étape qu'un utilisateur Windows

Sous Windows :
Sous Windows, vous devez localiser sur le fichier `php.ini` la ligne `;extension=gd` et supprimer le `;`. Si vous n'avez pas cette ligne, vous pouvez simplement ajouter cette ligne dans le fichier :
```
extension=gd
```

>Veuillez noter que GD peut déjà être installé sur la version par défaut de php, je recommande donc d'exécuter l'application en premier, et s'il y a une erreur, essayez d'installer GD

### Configurer Laravel

Pour cela, vous devez être dans le dossier `Coco_Detective`. Vous pouvez y naviguer avec la commande suivante :
```
cd cocodetective
```

Installer composer via un terminal de commande sous Linux :
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --filename=composer
php -r "unlink('composer-setup.php');"
```

Mettre à jour les dépendance de composer :
```
composer update
```

Installez les dépendances PHP via Composer :
```
composer install
```

Ensuite, il faut installer OnnxRuntime pour PHP avec les commandes suivantes :
```
composer require ankane/onnxruntime
composer exec -- php -r "require 'vendor/autoload.php'; OnnxRuntime\Vendor::check();"
```

Installez npm :
```
sudo apt-get install npm
```

Installez les dépendances JavaScript via npm :
```
npm install
```

Créez un fichier .env en dupliquant le fichier .env.example. Vous pouvez le faire avec la commande suivante :
```
cp .env.example .env
```

Une fois fait, il faut modifier plusieurs valeurs dans ce nouveau fichier `.env`. Les valeurs à modifier sont les suivantes :

- `APP_ENV` doit être définie à `local`, car elle va permettre à Composer de savoir quelles dépendances installer pour l'application lorsquelle est en développement
- `APP_DEBUG` doit être définie à `true`, car nous voulons voir les erreurs Laravel en environement de développement
- `APP_URL` doit contenir l'url de l'application Laravel

Ensuite, il faut modifier les informations relatives à la base de donnée :

- `DB_CONNECTION` doit être définie avec le type de base de donnée que l'application utiliser. Par exemple `mysql`
- `DB_HOST` sert à définir l'adresse du serveur où se trouve la base de données
- `DB_PORT` sert à définir le port sur lequel la connection devra être établie
- `DB_DATABASE` sert à définir le nom de la base de données 
- `DB_USERNAME` sert à définir le nom d'utilisateur pour pouvoir se connecter à la base de données
- `DB_PASSWORD` sert à déinir le mot de passe de l'utilisateur de la base de données

Ensuite, il faut laisser la valeur de `APP_KEY` vide. Il faut générer la valeur avec la commande suivante :
```
php artisan key:generate
```

Une fois que le fichier `.env` a été modifié avec les bonnes valeurs, il faut exécuter les migrations de bases de données :
```
php artisan migrate
````

Ensuite, liez le stockage, vous aurez ainsi les ressources nécessaires pour exécuter l'application :
```
php artisan storage:link
```

Démarrez le serveur de développement Laravel :
```
php artisan serve
````

Exécutez le serveur de développement npm sur une autre fenêtre de terminal :
```
npm run dev
````

Ces deux fenêtres doivent être ouvertes lorsque vous accédez à l'application. Lorsque vous souhaitez arrêter le serveur, vous pouvez simplement appuyer sur Ctrl + C

Visitez http://localhost:8000 dans votre navigateur pour accéder à l'application Vision.

## Dépendances

- [PHP](https://www.php.net/downloads.php)
- [Laravel](https://laravel.com/)
- [ONNXRuntime pour PHP](https://github.com/ankane/onnxruntime-php)
- [Compositeur](https://getcomposer.org/)
- [Node.js](https://nodejs.org/en/download)
