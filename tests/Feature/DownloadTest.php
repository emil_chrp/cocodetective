<?php

test('user can download the fie from the result page', function () {
    // Create data
    $testData = [];

    $testData[0]['classNumbers'][0] = 23;
    $testData[0]['classNumbers'][1] = 23;
    $testData[0]['classNumbers'][2] = 88;
    $testData[0]['classNames']['text'] = "";
    $testData[0]['classNames']['class'][0]['id'] = 23;
    $testData[0]['classNames']['class'][0]['name'] = "teddy bear";
    $testData[0]['classNames']['class'][0]['category'] = "animal";
    $testData[0]['classNames']['class'][1]['id'] = 23;
    $testData[0]['classNames']['class'][1]['name'] = "teddy bear";
    $testData[0]['classNames']['class'][1]['category'] = "animal";
    $testData[0]['classNames']['class'][2]['id'] = 88;
    $testData[0]['classNames']['class'][2]['name'] = "bear";
    $testData[0]['classNames']['class'][2]['category'] = "indoor";
    $testData[0]['imagePath'] = public_path('storage/Testing_data/Bears_analysed.jpg');
    $testData[1]['classNumbers'][0] = 23;
    $testData[1]['classNames']['text'] = "";
    $testData[1]['classNames']['class'][0]['id'] = 18;
    $testData[1]['classNames']['class'][0]['name'] = "dog";
    $testData[1]['classNames']['class'][0]['category'] = "animal";
    $testData[1]['imagePath'] = public_path('storage/Testing_data/Dog_analysed.jpg');

    $jsonTestData = json_encode($testData);

    $response = $this->post('/download', [
        'imagesInformations' => $jsonTestData
    ]);

    $response->assertStatus(200);

    $response->assertDownload();
});
