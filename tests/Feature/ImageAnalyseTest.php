<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\ClassificationService;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ImagePrediction;

test('test that the app store the image on the server and the model return the right response', function () {
    $this->withoutExceptionHandling();

    // Create a user
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Get the file
    $filePath = 'tests/Data/Train.jpeg';
    $fileExtension = 'jpeg';

    // Run thee model
    $classification = new ClassificationService;

    $responses = [];

    $responses = $classification->classify($filePath, $fileExtension);

    // Check that the model have found the bird class for the bird image
    expect($responses[0]['classNames']['class'][0]['name'])->toBe("train");

    // Check that the file is stored locally
    $this->assertFileExists(public_path($responses[0]['imagePath']));

    unlink(public_path($responses[0]['imagePath']));

    // Test if the record has been found on the database

    // $file = Storage::get('public/Testing_data/Train.jpeg');

    // // Send a post request with the upploaded file
    // $response = $this->post('classify', ['image' => $file]);

    // // Check that the answere is a success
    // $response->assertStatus(200);

    // // Vérifier que le fichier a été correctement stocké
    // $this->assertDatabaseHas('images', [
    //     'user_id' => $user->id
    // ]);
});
