<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\ClassificationService;
test('test that the app store the images on the server and the model return the right responses', function () {
    // Create a user
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Get the file
    $filePath = 'tests/Data/Test_Images.zip';
    $fileExtension = 'zip';

    // Run thee model
    $classification = new ClassificationService;

    $responses = [];

    $responses = $classification->classify($filePath, $fileExtension);

    // Check that the model have found the bird class for the bird image
    expect($responses[0]['classNames']['class'][0]['name'])->toBe("dog");
    expect($responses[1]['classNames']['class'][0]['name'])->toBe("train");

    // Check that the file is stored locally
    $this->assertFileExists(public_path($responses[0]['imagePath']));
    $this->assertFileExists(public_path($responses[1]['imagePath']));

    unlink(public_path($responses[0]['imagePath']));
    unlink(public_path($responses[1]['imagePath']));
});
