import './bootstrap';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();


let body = document.querySelector('#body');
let loader = document.querySelector('#loader');
let form = document.querySelector('#classifyForm')

form.addEventListener('submit', () => {
    window.scrollTo(0, 0);
    loader.classList.remove("hidden");
    body.classList.add('overflow-hidden');
});
