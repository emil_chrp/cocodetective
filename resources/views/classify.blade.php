<!--
    - ETML
    - Author : Emilien Charpié
    - Created at : 02.05.2024
    - Updated at : 06.05.2024

    - Description : This file is the view that is displayed when a user wants to classify images
 -->
<x-app-layout>
    <!-- Start section -->
    <section class="bg-white">
        <div class="grid max-w-screen-xl px-4 pt-20 pb-8 mx-auto md:py-16 md:pt-28">

            <!-- Start Title -->
            <div class="mr-auto place-self-center w-full">
                <h1 class="w-full text-center mb-4 text-4xl font-extrabold leading-none tracking-tight md:text-5xl xl:text-6xl">
                    Analyser une image <br>
                    Rapidement & Simplement
                </h1>
                @guest
                <p class="w-full text-center mb-6 font-light text-gray-500 md:mb-8 md:text-lg">
                    En tant qu'utilisateur non-connecté, vous disposez d'une analyse d'image par minute. <br>
                    Vous pouvez vous connecter pour en avoir d'avantage.
                </p>
                @endguest
                @auth
                <p class="w-full text-center mb-6 font-light text-gray-500 md:mb-8 md:text-lg">
                    Vous pouvez faire analyser autant d'images que vous le souhaitez. <br>
                    Pour téléverser plusieurs images, vous pouvez séléctionner un fichier zip.
                </p>
                @endauth
            </div>
            <!-- End title -->
            <!-- Start form -->
            <div>
                <x-form>
                    <form method="POST" action="{{ route('classify') }}" class="w-full" enctype="multipart/form-data" id="classifyForm">
                        @csrf
                        @if(isset($error))
                            <div class="m-1 mb-16 border-red-100 border-solid border-2 bg-red-100 text-red-500 rounded-md py-1">
                                {{$error}}
                            </div>
                        @endif
                        <div class="flex items-center justify-around">
                            {{-- <label for="image" class="border border-black border-solid p-4 cursor-pointer inline-block">
                                <input type="file" name="image"class="hidden"/>
                                Choisir un fichier
                            </label> --}}
                            <input type="file" name="file">
                        </div>
                        <div class="flex flex-col items-center justify-around mt-4">
                            <x-primary-button class="mt-8">
                                {{ __('Analyser') }}
                            </x-primary-button>
                        </div>
                    </form>
                </x-form>
            </div>
            <!-- End form -->
        </div>
    </section>
    <!-- End section -->
</x-app-layout>
