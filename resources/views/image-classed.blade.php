<!--
    - ETML
    - Author : Emilien Charpié
    - Created at : 02.05.2024
    - Updated at : 06.05.2024

    - Description : This file is the view, that is displayed when a user have analyse an image
 -->
 <x-app-layout>
    <!-- Start section -->
    <section class="bg-white min-h-[100vh]">
        <div class="flex mt-10 ml-6">
            <form action={{ route('download') }} method="POST" enctype="multipart/form-data">
                @csrf
                <?php
                    $jsonResponses = json_encode($responses);
                ?>
                <input type="hidden" name="imagesInformations" value="{{$jsonResponses}}">
                <button type="submit" class="inline-flex items-center justify-center w-full px-5 py-3 text-sm font-medium text-center text-gray-900 border border-gray-200 rounded-lg md:w-auto hover:bg-gray-100 focus:ring-4 focus:ring-gray-100">
                    Télécharger les images
                </button>
            </form>
        </div>
        <div class="grid max-w-screen-xl px-4 pt-10 pb-8 mx-auto md:py-16 md:pt-28">
            <!-- Start Title -->
            <div class="mr-auto place-self-center w-full">
                <h1 class="w-full text-center mb-4 text-4xl font-extrabold leading-none tracking-tight md:text-5xl">
                    Résultats de l'analyse
                </h1>
            </div>
            <!-- End title -->
        </div>
            @for ($x = 0; $x < count($responses); $x++)
                <div class="p-6 m-20 rounded-lg flex flex-col justify-around shadow">
                    @if (isset($responses[$x]['imagePath']))
                        <div class="flex justify-around m-6">
                            <div class="w-96 flex justify-around">
                                <img src={{ $responses[$x]['imagePath'] }}>
                            </div>
                        </div>
                    @endif
                    <div class="flex justify-around">
                        @if (isset($responses[$x]['classNames']['class']))
                            <table class="table-fixed w-1/2">
                                <thead>
                                    <tr class="bg-blue-100">
                                        <th>Numéro</th>
                                        <th>Nom</th>
                                        <th>Catégorie</th>
                                    </tr>
                                </thead>
                                <tbody class="[&>*:nth-child(odd)]:bg-gray-100">
                                    @for ($y = 0; $y < count($responses[$x]['classNames']['class']); $y++)
                                        <tr class="text-center">
                                            <td>
                                                {{$responses[$x]['classNumbers'][$y]}}
                                            </td>
                                            <td>
                                                {{$responses[$x]['classNames']['class'][$y]['name']}}
                                            </td>
                                            <td>
                                                {{$responses[$x]['classNames']['class'][$y]['category']}}
                                            </td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        @endif
                    </div>
                    {{-- @if (isset($responses[$x]['classNames']['class']))
                        <table class="w-60 bg-black">
                            <thead>
                                <td class="w-44 text-center font-bold">Numéro</td>
                                <td class="w-44 text-center font-bold">Nom</td>
                                <td class="w-44 text-center font-bold">Catégorie</td>
                            </thead>
                            <tbody>
                                @for ($y = 0; $y < count($responses[$x]['classNames']['class']); $y++)
                                    <tr>
                                        <td class="w-44 text-center">
                                            {{$responses[$x]['classNumbers'][$y]}}
                                        </td>
                                        <td class="w-44 text-center">
                                            {{$responses[$x]['classNames']['class'][$y]['name']}}
                                        </td>
                                        <td class="w-44 text-center">
                                            {{$responses[$x]['classNames']['class'][$y]['category']}}
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    @endif --}}
                </div>
            @endfor
        </div>
    </section>
</x-app-layout>
