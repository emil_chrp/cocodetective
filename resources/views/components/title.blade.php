<section class="relative">
    <div class="h-1/5 flex flex-row justify-center">
        <div class="">
            <h1 class="mt-28 mb-4 text-5xl font-extrabold tracking-tight text-gray-900 ">
                {{ $slot }}
            </h1>
        </div>
    </div>
</section>
