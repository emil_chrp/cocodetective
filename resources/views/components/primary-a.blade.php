<a {{ $attributes->merge(['type' => 'submit', 'class' => 'my-0 inline-flex items-center px-4 py-2 bg-purple-700 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-purple-600 focus:bg-purple-700 active:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-700 focus:ring-offset-2 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</a>
