<!--
    - ETML
    - Author : Emilien Charpié
    - Created at : 02.05.2024
    - Updated at : 06.05.2024

    - Description : This file is the layout for all the website. It contains links for the stylesheets,
    The navigation menu and the footer
 -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Coco Detective</title>
        <meta name="description" content="Image clssification with AI" />
        @vite('resources/css/app.css')
    </head>

    <body class="bg-white" id="body">
        <section class="hidden absolute w-full h-full bg-white z-50" id="loader">
            <div class="w-full h-full flex flex-col justify-around">
                <div>
                    <div class="flex justify-around">
                        <div class="w-1/4 flex flex-row justify-around">
                            <img src='storage/images/loader.gif' alt="loader">
                        </div>
                    </div>
                    <div class="w-full text-center text-4xl font-extrabold">
                        <h1>Coco Détective est entrain d'analyser votre requête...</h1>
                    </div>
                </div>
            </div>
        </section>
        <header class="w-full">
            <nav x-data="{ open: false }" class="bg-white border-gray-700 border-b py-7">
                <div class="flex items-center justify-between px-4 mx-auto">
                    <!-- Website Logo -->
                    <a href={{ route ('home') }} class="flex items-center">
                        <span class="self-center text-xl font-semibold whitespace-nowrap">Coco Detective</span>
                    </a>
                    <!-- Start Desktop Navigation -->
                    <div class="hidden md:flex" id="mobile-menu-2">
                        <x-nav-link :href="route('home')" :active="request()->routeIs('home')">
                            {{ __('Accueil') }}
                        </x-nav-link>
                        <x-nav-link :href="route('classify')" :active="request()->routeIs('classify')">
                            {{ __('Classer des images') }}
                        </x-nav-link>
                    </div>
                    @guest
                    <div class="hidden md:flex md:items-center md:ms-6">
                        <x-primary-a href="{{ route('login') }}" class="mx-2">Se connecter</x-primary-a>
                        <x-secondary-a href="{{ route('register') }}" class="mx-2">S'inscrire</x-secondary-a>
                    </div>
                    @endguest
                    @auth
                    <!-- Settings Dropdown -->
                    <div class="hidden md:flex md:items-center md:ms-6">
                        <x-dropdown align="right" width="48">
                            <x-slot name="trigger">
                                <button class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition ease-in-out duration-150">
                                    <div>{{ Auth::user()->name }}</div>

                                    <div class="ms-1">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </div>
                                </button>
                            </x-slot>

                            <x-slot name="content">
                                <x-dropdown-link :href="route('profile.edit')">
                                    {{ __('Profil') }}
                                </x-dropdown-link>

                                <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-dropdown-link :href="route('logout')"
                                            onclick="event.preventDefault();
                                                        this.closest('form').submit();">
                                        {{ __('Se déconnecter') }}
                                    </x-dropdown-link>
                                </form>
                            </x-slot>
                        </x-dropdown>
                    </div>
                    @endauth
                    <!-- End Desktop Navigation -->
                    <!-- Hamburger -->
                    <div class="-me-2 flex items-center md:hidden">
                        <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                            <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                                <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                </div>
                <!-- Start Responsive Navigation -->
                <div :class="{'block': open, 'hidden': ! open}" class="hidden md:hidden">
                    <div class="pt-2 pb-3 space-y-1">
                        <x-responsive-nav-link :href="route('home')" :active="request()->routeIs('home')">
                            {{ __('Home') }}
                        </x-responsive-nav-link>
                        <x-responsive-nav-link :href="route('classify')" :active="request()->routeIs('classify')">
                            {{ __('Classify Images') }}
                        </x-responsive-nav-link>
                    </div>

                    @auth
                    <!-- Responsive Settings Options -->
                    <div class="pt-4 pb-1 border-t border-gray-200">
                        <div class="px-4">
                            <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                            <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
                        </div>

                        <div class="mt-3 space-y-1">
                            <x-responsive-nav-link :href="route('profile.edit')" :active="request()->routeIs('profile.edit')">
                                {{ __('Profil') }}
                            </x-responsive-nav-link>

                            <!-- Authentication -->
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-responsive-nav-link :href="route('logout')"
                                        onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                    {{ __('Log Out') }}
                                </x-responsive-nav-link>
                            </form>
                        </div>
                    </div>
                    @endauth
                    @guest
                    <div class="pt-4 pb-1 border-t border-gray-200">
                        <x-primary-a href="{{ route('login') }}" class="mx-2">Se connecter</x-primary-a>
                        <x-secondary-a href="{{ route('register') }}" class="mx-2">S'inscrire</x-secondary-a>
                    </div>
                    @endguest
                </div>
                <!-- End Responsive Navigation -->
            </nav>
        </header>
        <main class="overflow-hidden">

        {{ $slot }}

        </main>
        <!-- Footer -->
        <footer class="border">
            <div class="max-w-6xl mx-auto px-6 py-16 text-gray-600 2xl:px-0 justify-around flex">
                <span class="text-gray-600">&copy; Emilien Charpié 2024 - TPI</span>
            </div>
        </footer>
        @vite('resources/js/app.js')
    </body>
</html>

